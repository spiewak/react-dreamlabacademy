import React, {Component} from 'react';
import SearchLoadingBar from './SearchLoadingBar';
import ResultSearchList from './ResultSearchList';
import BookAPIFetcher from './../services/BookAPIFetcher';
import SearchInputNavigation from './SearchInputNavigation';

export class BookSearchApp extends Component {
    constructor(props){
        super(props);
        this.state = {
                books: [],
                searchedText : 'Harry Potter',
                isLoading: false  
        };
        this.performSearch = this.performSearch.bind(this);
    }

    componentDidMount(){
           this.search(); 
    }

    search(){
        BookAPIFetcher.searchBooks(this.state.searchedText)
                .then((books) => {
                    this.setState({
                        books: books
                    })
                });
    }

    performSearch(value){
            this.setState({
                searchedText : value
            }, ()=>{
                this.search();
            })
    }

    render() {
        return (
            <div>
                <SearchInputNavigation 
                    initialPhase={this.state.searchedText}
                    onPhaseChanged={this.performSearch.bind(this)}
                      />
                <SearchLoadingBar 
                    isLoading={this.state.isLoading} />
                <ResultSearchList items={this.state.books} />    
            </div>
        )
    }
}

export default BookSearchApp;
