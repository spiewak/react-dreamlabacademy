import React, {Component, PropTypes} from 'react';
import _ from 'lodash';

export class SearchInputNavigation extends Component {
    constructor(props) {
        super(props);
        this.delayedCallback = _.debounce((event) => {
            this.props.onPhaseChanged(this.inputRef.value)
        }, 300); 
    }

    onChange(event) {
        this.delayedCallback(event);
    }

    render() {
        return (
            <div className="form-group">
                <label>Phrase to search</label>
                <input 
                    className="form-control" 
                    type="text" 
                    defaultValue={this.props.initialPhase} 
                    placeholder="Please write here searched phrase" 
                    ref={(ref) => { this.inputRef = ref }}
                    onChange={this.onChange.bind(this)} />
            </div>
        );
    }
}


const {string, func} = PropTypes;

SearchInputNavigation.propTypes = {
    initialPhase: string.isRequired,
    onPhaseChanged: func.isRequired
};

export default SearchInputNavigation;